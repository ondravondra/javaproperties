# JavaProperties #

The JavaProperties C# library provides support for parsing key value pairs stored in `.properties` files known from Java.
This allows using well known format for localization of ASP.NET apps and javascript client code.

## Features ##

* Work in progress. The features will be implemented soon. *

* Parsing `.properties` file or embedded resource into Dictionary<string, string>
* Creating tree based on keys with given separator, e.g. `my.sample.key = a` translates to `{ "my": { "sample" : { "key" : "a" } } }`
 * This is used to create resource object for the i18next javascript library (http://i18next.com/)
* `IResourceProvider` implementation for ASP.NET