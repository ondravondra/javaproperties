///<reference path="../../java-properties-i18n/dist/java-properties-i18n.d.ts"/>

(function () {
  var jpi18n = require('java-properties-i18n');

  var processedElementTag = 'java-properties-i18n';

  var containerVar: string = null;
  var defaultLang: string = null;

  function init(): boolean {
    var scripts = document.getElementsByTagName('script');
    for (var i = 0; i < scripts.length; i ++) {
      var cv = scripts[i].getAttribute('data-container-var');
      if (!cv) {
        continue;
      }

      containerVar = cv;
      defaultLang = scripts[i].getAttribute('data-default-lang');
    }

    return !!containerVar;
  }

  function processResourceElements() {
    var elems = document.getElementsByTagName('script');
    for (var i = 0; i < elems.length; i ++) {
      if ('text/x-java-properties' == elems[i].getAttribute('type') && !(processedElementTag in elems[i])) {
        processResourceElement(elems[i]);
        (elems[i] as any)[processedElementTag] = true;
      }
    }
  }

  function processResourceElement(el: HTMLScriptElement) {

    var src = el.getAttribute('src');

    var rxName = /[\/\\]?([^\/\\_]+)(_([^\/\\]+))?\.properties(\?[^\?]*)?$/;
    var mName = rxName.exec(src);
    if (!mName) {
      return;
    }

    let bun = mName[1];
    let lng = mName[3];
    if (lng) {
      lng = lng.replace(/_/g, '-');
    } else {
      lng = defaultLang || '_';
    }

    downloadResource(src, data => {
      var cont: any = null;
      if (!window.hasOwnProperty(containerVar)) {
        (window as any)[containerVar] = {};
      }

      cont = (window as any)[containerVar];

      var p = new jpi18n.Properties();
      p.load(jpi18n.readString(data));

      cont[lng] = cont[lng] || {};
      cont[lng][bun] = p.treeify();
    });
  }

  function getXHR(): XMLHttpRequest {
    return new XMLHttpRequest();
  }

  function downloadResource(src: string, cb: (data:string) => void): void {
    var xhr = getXHR();

    xhr.open('get', src, false);
    xhr.onload = function () {
      if (!('status' in xhr) || (xhr.status == 200)) {
        cb(xhr.responseText);
      }
    };

    xhr.onerror = function () { };

    // ie9 https://social.msdn.microsoft.com/Forums/ie/en-US/30ef3add-767c-4436-b8a9-f1ca19b4812e/ie9-rtm-xdomainrequest-issued-requests-may-abort-if-all-event-handlers-not-specified?forum=iewebdevelopment
    xhr.onprogress = function () { };

    xhr.send();
  }

  if (init()) {
    processResourceElements();
  }
})();