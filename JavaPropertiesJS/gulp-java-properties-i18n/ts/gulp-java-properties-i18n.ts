﻿///<reference path="../../typings/node/node.d.ts"/>
///<reference path="../../java-properties-i18n/dist/java-properties-i18n.d.ts"/>

var through = require('through2');
var jp = require('java-properties-i18n')

function jpi18n(opts?: { containerVar?: string, defaultLang?: string }) {

  return through.obj(function (file: any, enc: any, cb: any) {
    opts = opts || {};

    if (file.isNull()) {
      return cb(null, file);
    }

    var rxName = /(.*[\/\\])?([^\/\\_]+)(_([^\/\\]+))?\.properties$/;
    var mName = rxName.exec(file.path);
    if (!mName) {
      return cb(null, file);
    }

    var pat = mName[1] || '';
    var bun = mName[2];
    var lng = mName[4];
    if (lng) {
      lng = lng.replace(/_/g, '-');
    } else {
      lng = opts.defaultLang || '_';
    }

    if (file.isStream()) {
      return cb(new Error('gulp-java-properties-i18n: Streaming not supported'));
    }

    var str = file.contents.toString('utf-8');

    var p = new jp.Properties();
    p.load(jp.readString(str));
    var tree = p.treeify();

    var cnts = JSON.stringify(tree);
    if (opts.containerVar) {
      let cnts = 'var ' + opts.containerVar + ';';
      cnts += opts.containerVar + '=' + opts.containerVar + '||{};'
      cnts += opts.containerVar + '.' + lng + '=' + opts.containerVar + '.' + lng + '||{};';
      cnts += opts.containerVar + '.' + lng + '.' + bun + '=';
      cnts += JSON.stringify(tree) + ';';

      file.contents = new Buffer(cnts);
      file.path += '.js';
    } else {
      file.contents = new Buffer(JSON.stringify(tree));
      file.path = pat + bun + '-' + lng + '.json';
    }
    cb(null, file);
  });
}

export = jpi18n;