///<reference path="../../typings/node/node.d.ts"/>
///<reference path="../../java-properties-i18n/dist/java-properties-i18n.d.ts"/>

var fs = require('fs');
var path = require('path');
var jp = require('java-properties-i18n');

export interface ParsedBundleInfo {
  dir: string;
  bundle: string;
  language: string;
}

export function parseName(f: string, defaultLang?: string): ParsedBundleInfo {
  var rxName = /(.*[\/\\])?([^\/\\_]+)(_([^\/\\]+))?\.properties$/;
  var mName = rxName.exec(f);
  if (!mName) {
    return null;
  }

  var r = {
    dir: mName[1] || '',
    bundle: mName[2],
    language: mName[4]
  }

  if (r.language) {
    r.language = r.language.replace(/_/g, '-');
  } else if (defaultLang) {
    r.language = defaultLang;
  } else {
    return null;
  }

  return r;
}

export interface BundleProperties {
  [l: string]: /* Properties */ any;
}

function iterate<T>(arr: T[], action: (it: T, cb: Function) => void, done: Function) {
  if (arr && arr.length) {
    action(arr[0], () => iterate(arr.slice(1), action, done));
  } else {
    done();
  }
}

export function stripBOM(s: string): string {
  return s ? s.replace(/^\uFEFF/, '') : s;
}

export function loadBundles(
  firstPath: string, firstContent: string, defaultLang: string, mergeLanguages: string[],
  callback: (err: any, result?: { firstBundle: ParsedBundleInfo, bundles: BundleProperties, deps: string[] }) => void) {

  var parsed = parseName(firstPath, defaultLang || '_');

  var bundles: BundleProperties = {};
  var deps: string[] = [];

  (bundles[parsed.language] = new jp.Properties()).load(jp.readString(firstContent));

  var d = path.dirname(firstPath);
  var files = fs.readdir(d, (err: any, files: string[]) => {
    if (err) {
      return callback(err);
    }

    iterate(files, (f, cb) => {
      var fp = path.resolve(d, f);
      if (!fs.statSync(fp).isFile()) {
        cb();
        return;
      }

      var r = parseName(f, defaultLang || '_');

      if (!r || r.bundle !== parsed.bundle || r.language === parsed.language) {
        cb();
        return;
      }

      if (mergeLanguages && mergeLanguages.indexOf(r.language) === -1) {
        cb();
        return;
      }

      deps.push(fp);

      fs.readFile(fp, { encoding: 'utf-8' }, (err2: any, data: string) => {
        if (err2) {
          callback(err);
          return;
        }

        (bundles[r.language] = new jp.Properties()).load(jp.readString(stripBOM(data)));

        cb();
      });

    }, () => {
      callback(null, { firstBundle: parsed, bundles: bundles, deps: deps });
    });
   
  });
}

export interface BundleTree {
  [l: string]: {[b: string]: any}
}

export function treeifyBundles(name: string, bundles: BundleProperties): BundleTree {
  var res: BundleTree = {};
  for (var l in bundles) {
    if (bundles.hasOwnProperty(l)) {
      (res[l] = {})[name] = bundles[l].treeify();
    }
  }

  return res;
}

export function mergeBundles(
  firstBundle: ParsedBundleInfo, bundles: BundleProperties, mergeLanguages: string[],
  callback: (err: any, result?: { bundles: BundleProperties }) => void) {
  
  var resBundles: BundleProperties = {};
  resBundles[firstBundle.language] = bundles[firstBundle.language];

  mergeLanguages.forEach(ml => {
    if (!(ml in bundles)) {
      return;
    }

    resBundles[firstBundle.language].mergeFrom(bundles[ml], false);
  });

  callback(null, { bundles: resBundles });
}
