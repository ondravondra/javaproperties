﻿///<reference path="../../typings/node/node.d.ts"/>

var loaderUtils = require("loader-utils");
import { loadBundles, treeifyBundles, mergeBundles, ParsedBundleInfo, BundleProperties } from './loader';

const optionsKey = 'javaPropertiesI18nLoader';

export = function (source: string) {
  this.cacheable();
  var callback = this.async();

  var pQuery = loaderUtils.parseQuery(this.query);
  var pResourceQuery = loaderUtils.parseQuery(this.resourceQuery);

  var defaultLang: string = pResourceQuery['defaultLang'] || pQuery['defaultLang'] || (this.options[optionsKey] && this.options[optionsKey]['defaultLang']);
  var mergeLangs: string = pResourceQuery['mergeLangs'] || pQuery['mergeLangs'] || (this.options[optionsKey] && this.options[optionsKey]['mergeLangs']);

  var mergeLanguages = mergeLangs ? mergeLangs.split(',') : null;

  loadBundles(
    this.resourcePath, source, defaultLang, mergeLanguages,
    (err: any, result?: { firstBundle: ParsedBundleInfo, bundles: BundleProperties, deps: string[] }) => {

      if (err || !result) {
        callback(err || 'err');
      }

      if (result.deps) {
        result.deps.forEach(fp => this.addDependency(fp));
      }

      if (!mergeLanguages) {
        callback(null, 'module.exports = ' + JSON.stringify(treeifyBundles(result.firstBundle.bundle, result.bundles)) + ';');
      } else {
        mergeBundles(result.firstBundle, result.bundles, mergeLanguages, (err2: any, result2?: { bundles: BundleProperties }) => {
          if (err2 || !result2) {
            callback(err2 || 'err');
          }

          callback(null, 'module.exports = ' + JSON.stringify(treeifyBundles(result.firstBundle.bundle, result2.bundles)) + ';');
        });
      }
    });
};