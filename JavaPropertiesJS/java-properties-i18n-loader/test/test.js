﻿var should = require('should');
var expect = require('chai').expect;

var path = require('path');
var fs = require('fs');

var ldr = require('../dist/loader');

describe('java-properties-i18n-loader', function () {

  describe('treeifyBundles', function () {
    it('should treeify test_a and test_b together', function (done) {
      var pathA = path.resolve(__dirname, 'test_a.properties');
      var pathB = path.resolve(__dirname, 'test_b.properties');
      var contentA = ldr.stripBOM(fs.readFileSync(pathA, 'utf-8')); 

      ldr.loadBundles(pathA, contentA, 'a', null, function (err, result) {
        var tree = ldr.treeifyBundles(result.firstBundle.bundle, result.bundles);
        expect(tree).to.deep.equal({
          a: {
            test: {
              only_a: 'a',
              both_a_b: 'a',
              multi: {
                level: {
                  path: {
                    a: 'a',
                    a_b: 'a'
                  }
                }
              }
            }
          },
          b: {
            test: {
              only_b: 'b',
              both_a_b: 'b',
              multi: {
                level: {
                  path: {
                    b: 'b',
                    a_b: 'b'
                  }
                }
              }
            }
          }
        });
        done();
      });
    });
  });

  describe('mergeBundles', function () {
    it('should produce bundle a with all values from test_a and selected values from test_b which are not present in test_a', function (done) {
      var pathA = path.resolve(__dirname, 'test_a.properties');
      var pathB = path.resolve(__dirname, 'test_b.properties');
      var contentA = ldr.stripBOM(fs.readFileSync(pathA, 'utf-8'));
      var mergeLanguages = ['b'];      

      ldr.loadBundles(pathA, contentA, 'a', mergeLanguages, function (err, result) {
        ldr.mergeBundles(result.firstBundle, result.bundles, mergeLanguages, function (err2, result2) {
          expect(result2.bundles).to.have.all.keys(['a']);
          expect(result2.bundles.a.items).to.deep.equal({
            only_a: 'a',
            both_a_b: 'a',
            only_b: 'b',
            'multi.level.path.a': 'a',
            'multi.level.path.a_b': 'a',
            'multi.level.path.b': 'b'
          });
          done();
        });
      });
    })
  });

});
