﻿import {PropertiesTextReader, ReadChar} from './PropertiesTextReader';
import {PropertiesParser} from './PropertiesParser';

export default class Properties {

  items: { [key: string]: string } = {};

  load(input: ReadChar): void {
    var reader = new PropertiesTextReader(input);
    var parser = new PropertiesParser();

    var decl: any;
    while ((decl = reader.readDeclaration()) != null) {
      var kv = parser.parseDeclaration(decl);
      if (kv.key == null) {
        continue;
      }

      this.items[kv.key] = kv.value;
    }
  }

  treeify(separator: string = '.'): any {
    var root = {};

    for (var k in this.items) {
      this.treeifyBranch(k, this.items[k], root, separator);
    }

    return root;
  }

  private treeifyBranch(key: string, value: string, parent: any, separator: string): void {
    var i = key.indexOf(separator);
    if (i == -1) {
      parent[key] = value;
      return;
    }

    var k = key.substring(0, i);
    var node: any;

    if (parent.hasOwnProperty(k)) {
      node = parent[k];
      if (typeof node !== 'object') {
        node = {};
        node[''] = parent[k];
        parent[k] = node;
      }
    } else {
      node = {};
      parent[k] = node;
    }

    this.treeifyBranch(key.substring(i + 1), value, node, separator);
  }

  mergeFrom(other: Properties, overwriteExisting: boolean) {
    for (var k in other.items) {
      if (!other.items.hasOwnProperty(k)) {
        continue;
      }

      if (!overwriteExisting && this.items.hasOwnProperty(k)) {
        continue;
      }

      this.items[k] = other.items[k];
    }
  }
}