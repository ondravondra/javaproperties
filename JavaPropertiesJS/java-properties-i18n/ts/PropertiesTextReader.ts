﻿///<reference path="../../typings/node/node.d.ts"/>

import * as stream from 'stream';

export interface ReadChar {
  readChar(): string;
}

export function readString(s: string): ReadChar {
  var pos = 0;
  return {
    readChar: function (): string {
      return s.length > pos ? s[pos++] : null;
    }
  };
}

export class PropertiesTextReader {
  private input: ReadChar;

  constructor(input: ReadChar) {
    this.input = input;
  }

  readDeclaration(): string {
    var ch: string;
    var hadCR = false;
    var isNewLine = true;
    var hadBackSlash = false;
    var hasSplitLine = false;
    var sbr = '';
    while (ch = this.input.readChar()) {
      if (hadCR) {
        hadCR = false;
        if (ch == '\n') {
          continue;
        }
      }

      if (isNewLine) {
        if (ch == ' ' || ch == '\t' || ch == '\f') {
          continue;
        }

        if (!hasSplitLine && (ch == '\r' || ch == '\n')) {
          continue;
        }

        isNewLine = false;
        hasSplitLine = false;
      }

      if (ch == '\n' || ch == '\r') {
        if (sbr.length < 1 || sbr[0] == '#' || sbr[0] == '!') {
          sbr = '';
          isNewLine = true;
          continue;
        }

        if (hadBackSlash) {
          hasSplitLine = true;
          hadBackSlash = false;
          isNewLine = true;
          if (ch == '\r') {
            hadCR = true;
          }
        }
        else {
          return sbr;
        }

        continue;
      }

      if (ch == '\\') {
        if (hadBackSlash) {
          sbr += '\\';
          hadBackSlash = false;
        }
        else {
          hadBackSlash = true;
          continue;
        }
      }
      else {
        if (hadBackSlash) {
          sbr += '\\';
          hadBackSlash = false;
        }
      }

      // append the char
      sbr += ch;
    }

    if (sbr.length < 1 || sbr[0] == '#' || sbr[0] == '!') {
      return null;
    }

    return sbr;
  }
}
