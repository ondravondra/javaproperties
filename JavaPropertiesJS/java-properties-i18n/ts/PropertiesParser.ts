﻿export class PropertiesParser {

  parseDeclaration(decl: string): { key: string, value: string } {
    var kv = this.splitKeyAndValue(decl);
    if (!kv) {
      return { key: null, value: null };
    }

    return { key: this.decode(kv.key), value: this.decode(kv.value) };
  }

  private splitKeyAndValue(decl: string): { key: string, value: string } {
    var key: string = null;
    var value: string = null;

    var hadBackSlash = false;
    for (var i = 0; i < decl.length; i++) {
      let ch = decl[i];

      if (!hadBackSlash && (ch == '=' || ch == ':')) {
        key = decl.substring(0, i);
        value = this.findValue(decl.substring(i + 1), false);
        break;
      }

      if (!hadBackSlash && (ch == ' ' || ch == '\t' || ch == '\f')) {
        key = decl.substring(0, i);
        value = this.findValue(decl.substring(i + 1), true);
        break;
      }

      hadBackSlash = ch == '\\' && !hadBackSlash;
    }

    return key && value ? { key: key, value: value } : null;
  }

  private findValue(tail: string, ignoreSeparator: boolean): string {
    for (var i = 0; i < tail.length; i++) {
      let ch = tail[i];
      if (ignoreSeparator && (ch == '=' || ch == ':')) {
        ignoreSeparator = false;
        continue;
      }

      if (!(ch == ' ' || ch == '\t' || ch == '\f')) {
        return tail.substring(i);
      }
    }

    return '';
  }

  private decode(str: string): string {
    var ofs = 0;
    var sbr = '';
    while (ofs < str.length) {
      var ch = str[ofs++];
      if (ch != '\\') {
        sbr += ch;
        continue;
      }

      ch = str[ofs++];
      switch (ch) {
        case 'u':
          var code = parseInt(str.substring(ofs, 4), 16);
          ofs += 4;
          sbr += String.fromCharCode(code);
          break;
        case 't':
          sbr += '\t';
          break;
        case 'r':
          sbr += '\r';
          break;
        case 'n':
          sbr += '\n';
          break;
        case 'f':
          sbr += '\f';
          break;
        default:
          sbr += ch;
          break;
      }
    }

    return sbr;
  }
}