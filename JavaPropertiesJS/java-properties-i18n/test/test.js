﻿var should = require('should');
var expect = require('chai').expect;

var jp = require('..');

describe('JavaProperties', function () {

  describe('readString', function () {
    it('should read all charactes', function () {
      var rs = jp.readString('abcd');
      expect(rs.readChar()).equal('a');
      expect(rs.readChar()).equal('b');
      expect(rs.readChar()).equal('c');
      expect(rs.readChar()).equal('d');
      expect(rs.readChar()).equal(null);
    })
  });

  describe('PropertiesTextReader', function () {
    it('should read simple declaration', function () {
      var input = "abcd efg \\na";
      var expected = "abcd efg \\na";

      var re = new jp.PropertiesTextReader(jp.readString(input));

      expect(re.readDeclaration()).equal(expected);
      expect(re.readDeclaration()).equal(null);
    });
  });
});

console.log(jp);
