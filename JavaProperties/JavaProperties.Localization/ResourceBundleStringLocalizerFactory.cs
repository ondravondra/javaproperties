﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Reflection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Options;
using System.IO;
using System.Collections.Concurrent;

namespace JavaProperties.Localization
{
    public class ResourceBundleStringLocalizerFactory : IStringLocalizerFactory
    {
        private readonly ConcurrentDictionary<string, ResourceBundleStringLocalizer> localizerCache =
            new ConcurrentDictionary<string, ResourceBundleStringLocalizer>();
        private readonly IHostingEnvironment hostingEnvironment;
        private readonly string relativeNamespace;
        private readonly string relativePath;

        public ResourceBundleStringLocalizerFactory(
            IHostingEnvironment hostingEnvironment,
            IOptions<ResourceBundleLocalizationOptions> localizationOptions)
        {
            if (hostingEnvironment == null)
            {
                throw new ArgumentNullException(nameof(hostingEnvironment));
            }

            if (localizationOptions == null)
            {
                throw new ArgumentNullException(nameof(localizationOptions));
            }

            this.hostingEnvironment = hostingEnvironment;

            relativePath = localizationOptions.Value.FileResourcesPath;
            relativeNamespace = localizationOptions.Value.ResourcesPath ?? string.Empty;
            if (!string.IsNullOrEmpty(relativeNamespace))
            {
                relativeNamespace = relativeNamespace
                    .Replace(Path.AltDirectorySeparatorChar, '.')
                    .Replace(Path.DirectorySeparatorChar, '.') + ".";
            }
        }

        public IStringLocalizer Create(Type resourceSource)
        {
            if (resourceSource == null)
            {
                throw new ArgumentNullException(nameof(resourceSource));
            }

            var localizer = localizerCache.GetOrAdd(resourceSource.GetTypeInfo().FullName, _ =>
            {
                var typeInfo = resourceSource.GetTypeInfo();
                var assembly = typeInfo.Assembly;
                var typeName = TrimPrefix(typeInfo.FullName, hostingEnvironment.ApplicationName + ".");

                var bundle = ResourceBundle.Load(assembly, hostingEnvironment.ApplicationName + "." + (relativeNamespace ?? string.Empty) + typeName);

                if (bundle == null)
                {
                    bundle = CreateFromFile(typeName);
                }

                if (bundle == null)
                {
                    bundle = new ResourceBundleNotFound(typeName);
                }

                return new ResourceBundleStringLocalizer(bundle);
            });

            localizer.CheckSourceFilesAndReload();

            return localizer;
        }

        public IStringLocalizer Create(string baseName, string location)
        {
            if (baseName == null)
            {
                throw new ArgumentNullException(nameof(baseName));
            }

            var localizer = localizerCache.GetOrAdd($"B={baseName},L={location}", _ =>
            {
                var assemblyLoc = location ?? hostingEnvironment.ApplicationName;
                var assembly = Assembly.Load(new AssemblyName(assemblyLoc));
                var typeName = TrimPrefix(baseName, assemblyLoc + ".");

                var bundle = ResourceBundle.Load(assembly, assemblyLoc + "." + (relativeNamespace ?? string.Empty) + typeName);

                if (bundle == null)
                {
                    bundle = CreateFromFile(typeName);
                }

                if (bundle == null)
                {
                    bundle = new ResourceBundleNotFound(typeName);
                }

                return new ResourceBundleStringLocalizer(bundle);
            });

            localizer.CheckSourceFilesAndReload();

            return localizer;
        }

        private ResourceBundle CreateFromFile(string typeName)
        {
            var filePath = typeName.Replace('.', Path.DirectorySeparatorChar);

            var fileDir = Path.Combine(hostingEnvironment.ContentRootPath, Path.GetDirectoryName(filePath));
            if (!string.IsNullOrEmpty(relativePath))
            {
                fileDir = Path.Combine(fileDir, relativePath);
            }

            return ResourceBundle.Load(fileDir, true, Path.GetFileName(filePath));
        }

        private static string TrimPrefix(string name, string prefix)
        {
            if (name.StartsWith(prefix, StringComparison.Ordinal))
            {
                return name.Substring(prefix.Length);
            }

            return name;
        }
    }
}
