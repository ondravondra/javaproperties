﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace JavaProperties.Localization
{
    internal class ResourceBundleNotFound : ResourceBundle
    {
        internal ResourceBundleNotFound(string name) : base(name, new Dictionary<CultureInfo, Properties>(), string.Empty)
        {
        }

        public override bool CheckSourceFilesTimestamp()
        {
            return true;
        }

        public override ResourceBundle LoadCurrentVersion()
        {
            return new ResourceBundleNotFound(name);
        }
    }
}
