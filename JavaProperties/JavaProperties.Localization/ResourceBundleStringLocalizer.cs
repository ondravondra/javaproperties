﻿using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Globalization;
using System.Reflection;

namespace JavaProperties.Localization
{
    public class ResourceBundleStringLocalizer : IStringLocalizer
    {
        private ResourceBundle bundle;
        private readonly CultureInfo withCulture;

        public ResourceBundleStringLocalizer(ResourceBundle bundle, CultureInfo withCulture = null)
        {
            this.bundle = bundle;
            this.withCulture = withCulture;
        }

        public void CheckSourceFilesAndReload()
        {
            if (!bundle.CheckSourceFilesTimestamp())
            {
                bundle = bundle.LoadCurrentVersion();
            }
        }

        public LocalizedString this[string name]
        {
            get
            {
                var value = GetString(name, withCulture);
                return new LocalizedString(name, value ?? name, resourceNotFound: value == null);
            }
        }

        public LocalizedString this[string name, params object[] arguments]
        {
            get
            {
                var format = GetString(name, withCulture);
                var value = string.Format(format ?? name, arguments);
                return new LocalizedString(name, value, resourceNotFound: format == null);
            }
        }

        public IEnumerable<LocalizedString> GetAllStrings(bool includeParentCultures)
        {
            throw new NotImplementedException();
        }

        public IStringLocalizer WithCulture(CultureInfo culture)
        {
            return new ResourceBundleStringLocalizer(bundle, withCulture);
        }

        private string GetString(string name, CultureInfo culture)
        {
            return bundle.Get(name, culture ?? CultureInfo.CurrentUICulture);
        }
    }
}
