﻿using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JavaProperties.Localization
{
    public class ResourceBundleLocalizationOptions : LocalizationOptions
    {
        public string FileResourcesPath { get; set; }
    }
}
