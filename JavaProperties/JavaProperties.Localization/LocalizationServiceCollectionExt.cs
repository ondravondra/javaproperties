﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JavaProperties.Localization
{
    public static class LocalizationServiceCollectionExt
    {
        public static IServiceCollection AddJavaPropertiesLocalization(this IServiceCollection services)
        {
            AddLocalizationServices(services);

            return services;
        }

        public static IServiceCollection AddJavaPropertiesLocalization(this IServiceCollection services, Action<ResourceBundleLocalizationOptions> setupAction)
        {
            AddLocalizationServices(services);

            services.Configure(setupAction);

            return services;
        }

        internal static void AddLocalizationServices(IServiceCollection services)
        {
            services.TryAddSingleton<IStringLocalizerFactory, ResourceBundleStringLocalizerFactory>();
            services.TryAddTransient(typeof(IStringLocalizer<>), typeof(StringLocalizer<>));
        }
    }
}
