﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JavaProperties.ResourceProvider
{
    internal class FileResourceBundle : ResourceBundle
    {
        private readonly string directory;
        private readonly long[] timestamps;

        private FileResourceBundle(string directory, long[] timestamps, string name, Dictionary<CultureInfo, Properties> cultures, string timestamp) : base(name, cultures, timestamp)
        {
            this.directory = directory;
            this.timestamps = timestamps;
        }

        public override bool CheckSourceFilesTimestamp()
        {
            return CollectSourceFiles(directory, name).Select(sf => sf.Timestamp).SequenceEqual(timestamps);
        }

        public override ResourceBundle LoadCurrentVersion()
        {
            return LoadFileResourceBundle(directory, false, name);
        }

        private class SourceFile
        {
            public string Path { get; set; }
            public long Timestamp { get; set; }
            public string Culture { get; set; }
        }

        private static IEnumerable<SourceFile> CollectSourceFiles(string dir, string name)
        {
            var main = Path.Combine(dir, name + PROP_EXT);
            if (File.Exists(main))
            {
                yield return new SourceFile
                {
                    Path = main,
                    Timestamp = new FileInfo(main).LastWriteTimeUtc.Ticks,
                    Culture = null
                };
            }

            foreach (var f in Directory.GetFiles(dir, name + "_*" + PROP_EXT, SearchOption.TopDirectoryOnly).OrderBy(f => f))
            {
                yield return new SourceFile
                {
                    Path = f,
                    Timestamp = new FileInfo(f).LastWriteTimeUtc.Ticks,
                    Culture = Path.GetFileNameWithoutExtension(f).Substring(name.Length + 1).Replace("_", "-")
                };
            }
        }

        private static Properties LoadFile(string path)
        {
            try
            {
                using (var stream = File.OpenText(path))
                {
                    var p = new Properties();
                    p.Load(stream);
                    return p;
                }
            }
            catch
            {
                return null;
            }
        }

        private static FileResourceBundle LoadFiles(string dir, string name)
        {
            var timestamps = new List<long>();
            var cultures = new Dictionary<CultureInfo, Properties>();
            var sourceFiles = CollectSourceFiles(dir, name);

            var tssbr = new StringBuilder();

            foreach (var sf in sourceFiles)
            {
                var p = LoadFile(sf.Path);
                try
                {
                    cultures.Add(sf.Culture == null ? CultureInfo.InvariantCulture : new CultureInfo(sf.Culture), p);
                    timestamps.Add(sf.Timestamp);
                    tssbr.Append(TicksToB64(sf.Timestamp));
                }
                catch
                {
                }
            }

            return new FileResourceBundle(dir, timestamps.ToArray(), name, cultures, tssbr.ToString());
        }

        internal static FileResourceBundle LoadFileResourceBundle(string path, bool recursiveSearch, string name)
        {
            var dirs = new Queue<string>();

            if (!Directory.Exists(path))
            {
                return null;
            }

            dirs.Enqueue(path);
            while (dirs.Count > 0)
            {
                var p = dirs.Dequeue();

                try {
                    if (File.Exists(Path.Combine(p, name + PROP_EXT)))
                    {
                        return LoadFiles(p, name);
                    }

                    if (!recursiveSearch)
                    {
                        return null;
                    }

                    foreach (var s in Directory.GetDirectories(p))
                    {
                        dirs.Enqueue(s);
                    }
                }
                catch (PathTooLongException)
                {
                    // .net libraries can only handle 260 chars long path, its not future yet
                }
            }

            return null;
        }
    }
}
