﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace JavaProperties.ResourceProvider
{
    internal class AssemblyResourceBundle : ResourceBundle
    {
        private AssemblyResourceBundle(string name, Dictionary<CultureInfo, Properties> cultures, string timestamp) : base(name, cultures, timestamp)
        {
        }

        public override bool CheckSourceFilesTimestamp()
        {
            return true;
        }

        public override ResourceBundle LoadCurrentVersion()
        {
            return this;
        }

        private static Properties LoadResource(Assembly assembly, string name)
        {
            try
            {
                using (var stream = assembly.GetManifestResourceStream(name))
                {
                    using (var reader = new StreamReader(stream))
                    {
                        var p = new Properties();
                        p.Load(reader);
                        return p;
                    }
                }
            }
            catch
            {
                return null;
            }
        }

        internal static ResourceBundle LoadAssemblyResourceBundle(Assembly assembly, string name)
        {
            var tssbr = new StringBuilder();
            try
            {
                tssbr.Append(TicksToB64(File.GetCreationTime(assembly.Location).Ticks));
            }
            catch { }

            var resourceName = assembly.GetManifestResourceNames().FirstOrDefault(n => n.EndsWith(name + PROP_EXT));
            if (resourceName == null)
            {
                return null;
            }

            var cultures = new Dictionary<CultureInfo, Properties>();
            cultures.Add(CultureInfo.InvariantCulture, LoadResource(assembly, resourceName));
            if (cultures[CultureInfo.InvariantCulture] == null)
            {
                return null;
            }

            var baseName = resourceName.Substring(0, resourceName.Length - PROP_EXT.Length);
            foreach (var rn in assembly.GetManifestResourceNames().Where(n => n.StartsWith(baseName + "_") && n.EndsWith(PROP_EXT)))
            {
                var cn = rn.Substring(baseName.Length + 1, rn.Length - baseName.Length - PROP_EXT.Length - 1).Replace("_", "-");
                var p = LoadResource(assembly, rn);
                if (p != null)
                {
                    try
                    {
                        cultures.Add(new CultureInfo(cn), p);
                    }
                    catch
                    {
                    }
                }
            }

            return new AssemblyResourceBundle(name, cultures, tssbr.ToString());
        }
    }
}
