﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace JavaProperties.ResourceProvider
{
    public abstract class ResourceBundle : IEnumerable<KeyValuePair<CultureInfo, Properties>>
    {
        protected readonly string name;
        protected readonly Dictionary<CultureInfo, Properties> cultures;
        protected readonly string timestamp;

        internal ResourceBundle(string name, Dictionary<CultureInfo, Properties> cultures, string timestamp)
        {
            this.name = name;
            this.cultures = cultures;
            this.timestamp = timestamp;
        }

        public string Name { get { return name; } }

        public string Timestamp { get { return timestamp; } }

        public abstract bool CheckSourceFilesTimestamp();

        public abstract ResourceBundle LoadCurrentVersion();

        public IEnumerator<KeyValuePair<CultureInfo, Properties>> GetEnumerator()
        {
            return cultures.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return cultures.GetEnumerator();
        }

        public string Get(string resourceKey, CultureInfo culture)
        {
            while (true)
            {
                var props = cultures.ContainsKey(culture) ? cultures[culture] : null;
                if (props != null && props.ContainsKey(resourceKey))
                {
                    return props[resourceKey];
                }

                if (culture.Name.Length < 1)
                {
                    return null;
                }

                culture = culture.Parent;
            }
        }

        protected const string PROP_EXT = ".properties";

        protected static string TicksToB64(long ticks)
        {
            return Convert.ToBase64String(BitConverter.GetBytes(ticks)).Replace("=", "");
        }

        public static ResourceBundle Load(Assembly assembly, string name)
        {
            return AssemblyResourceBundle.LoadAssemblyResourceBundle(assembly, name);
        }

        public static ResourceBundle Load(string path, bool recursiveSearch, string name)
        {
            return FileResourceBundle.LoadFileResourceBundle(path, recursiveSearch, name);
        }
    }
}
