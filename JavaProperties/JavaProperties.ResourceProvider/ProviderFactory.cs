﻿using System;
using System.Collections.Generic;
using System.Web.Compilation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Globalization;

namespace JavaProperties.ResourceProvider
{
    public class ProviderFactory : ResourceProviderFactory
    {
        private class ResourceBundleNotFound : ResourceBundle
        {
            internal ResourceBundleNotFound(string name) : base(name, new Dictionary<CultureInfo, Properties>(), string.Empty)
            {
            }

            public override bool CheckSourceFilesTimestamp()
            {
                return true;
            }

            public override ResourceBundle LoadCurrentVersion()
            {
                return new ResourceBundleNotFound(name);
            }
        }

        public override IResourceProvider CreateGlobalResourceProvider(string classKey)
        {
            var ass = Assembly.GetEntryAssembly() ?? Assembly.GetCallingAssembly();

            var bundle = ass != null ? ResourceBundle.Load(ass, classKey) : null;

            if (bundle == null)
            {
                bundle = ResourceBundle.Load(System.Web.HttpRuntime.AppDomainAppPath, true, classKey);
            }

            return new GlobalProvider(bundle ?? new ResourceBundleNotFound(classKey));
        }

        public override IResourceProvider CreateLocalResourceProvider(string virtualPath)
        {
            throw new NotSupportedException("Local resource provider is not supported");
        }
    }
}
