﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Compilation;

namespace JavaProperties.ResourceProvider
{
    internal class GlobalProvider : IResourceProvider
    {
        private ResourceBundle bundle;

        public GlobalProvider(ResourceBundle bundle)
        {
            this.bundle = bundle;
        }

        public IResourceReader ResourceReader
        {
            get
            {
                throw new NotSupportedException("ResourceReader is not supported");
            }
        }

        public object GetObject(string resourceKey, CultureInfo culture)
        {
            ResourceBundle b;
            if (HttpContext.Current != null)
            {
                var ik = GetType().FullName + ".res." + bundle.Name;

                if (!HttpContext.Current.Items.Contains(ik))
                {
                    if (!bundle.CheckSourceFilesTimestamp())
                    {
                        bundle = bundle.LoadCurrentVersion();
                    }

                    HttpContext.Current.Items.Add(ik, bundle);
                    b = bundle;
                }
                else
                {
                    b = HttpContext.Current.Items[ik] as ResourceBundle;
                }
            }
            else
            {
                b = bundle;
            }

            return b.Get(resourceKey, culture ?? System.Threading.Thread.CurrentThread.CurrentUICulture);
        }
    }
}
