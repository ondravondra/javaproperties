﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JavaProperties
{
    internal class PropertiesTextReader
    {
        private readonly TextReader inputStream;
        private readonly char[] buffer = new char[4096];
        private int bufferContentOfs = 0;
        private int bufferContentSize = 0;

        public PropertiesTextReader(TextReader inputStream)
        {
            this.inputStream = inputStream;
        }

        private bool ReadChar(out char ch)
        {
            if (bufferContentOfs >= bufferContentSize)
            {
                bufferContentSize = inputStream.ReadBlock(buffer, 0, buffer.Length);
                bufferContentOfs = 0;
                if (bufferContentSize < 1)
                {
                    ch ='\0';
                    return false;
                }
            }

            ch = buffer[bufferContentOfs++];
            return true;
        }

        public string ReadDeclaration()
        {
            char ch;
            bool hadCR = false;
            bool isNewLine = true;
            bool hadBackSlash = false;
            bool hasSplitLine = false;
            var sbr = new StringBuilder();
            while (ReadChar(out ch))
            {
                if (hadCR)
                {
                    hadCR = false;
                    if (ch == '\n')
                    {
                        continue;
                    }
                }

                if (isNewLine)
                {
                    if (ch == ' ' || ch == '\t' || ch == '\f')
                    {
                        continue;
                    }

                    if (!hasSplitLine && (ch == '\r' || ch == '\n'))
                    {
                        continue;
                    }

                    isNewLine = false;
                    hasSplitLine = false;
                }

                if (ch == '\n' || ch == '\r')
                {
                    if (sbr.Length < 1 || sbr[0] == '#' || sbr[0] == '!')
                    {
                        sbr.Clear();
                        isNewLine = true;
                        continue;
                    }

                    if (hadBackSlash)
                    {
                        hasSplitLine = true;
                        hadBackSlash = false;
                        isNewLine = true;
                        if (ch == '\r')
                        {
                            hadCR = true;
                        }
                    }
                    else
                    {
                        return sbr.ToString();
                    }

                    continue;
                }

                if (ch == '\\')
                {
                    if (hadBackSlash)
                    {
                        sbr.Append('\\');
                        hadBackSlash = false;
                    }
                    else
                    {
                        hadBackSlash = true;
                        continue;
                    }
                }
                else
                {
                    if (hadBackSlash)
                    {
                        sbr.Append('\\');
                        hadBackSlash = false;
                    }
                }

                // append the char
                sbr.Append(ch);
            }

            if (sbr.Length < 1 || sbr[0] == '#' || sbr[0] == '!')
            {
                return null;
            }

            return sbr.ToString();
        }
    }
}
