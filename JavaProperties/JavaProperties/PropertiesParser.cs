﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JavaProperties
{
    internal class PropertiesParser
    {
        public KeyValuePair<string, string> ParseDeclaration(string decl)
        {
            string key, value;
            if (!SplitKeyAndValue(decl, out key, out value))
            {
                return new KeyValuePair<string, string>(null, null);
            }

            return new KeyValuePair<string, string>(Decode(key), Decode(value));
        }

        private bool SplitKeyAndValue(string decl, out string key, out string value)
        {
            key = value = null;

            var hadBackSlash = false;
            for (var i = 0; i < decl.Length; i++)
            {
                char ch = decl[i];

                if (!hadBackSlash && (ch == '=' || ch == ':'))
                {
                    key = decl.Substring(0, i);
                    value = FindValue(decl.Substring(i + 1), false);
                    break;
                }

                if (!hadBackSlash && (ch == ' ' || ch == '\t' || ch == '\f'))
                {
                    key = decl.Substring(0, i);
                    value = FindValue(decl.Substring(i + 1), true);
                    break;
                }

                hadBackSlash = ch == '\\' && !hadBackSlash;
            }

            return !string.IsNullOrEmpty(key) && !string.IsNullOrEmpty(value);
        }

        private string FindValue(string tail, bool ignoreSeparator)
        {
            for (var i = 0; i < tail.Length; i++)
            {
                char ch = tail[i];
                if (ignoreSeparator && (ch == '=' || ch == ':'))
                {
                    ignoreSeparator = false;
                    continue;
                }

                if (!(ch == ' ' || ch == '\t' || ch == '\f'))
                {
                    return tail.Substring(i);
                }
            }

            return string.Empty;
        }

        private string Decode(string str)
        {
            var ofs = 0;
            var sbr = new StringBuilder(str.Length);
            while (ofs < str.Length)
            {
                var ch = str[ofs++];
                if (ch != '\\')
                {
                    sbr.Append(ch);
                    continue;
                }

                ch = str[ofs++];
                switch (ch)
                {
                    case 'u':
                        var code = int.Parse(str.Substring(ofs, 4), System.Globalization.NumberStyles.HexNumber);
                        ofs += 4;
                        sbr.Append(Convert.ToChar(code));
                        break;
                    case 't':
                        sbr.Append('\t');
                        break;
                    case 'r':
                        sbr.Append('\r');
                        break;
                    case 'n':
                        sbr.Append('\n');
                        break;
                    case 'f':
                        sbr.Append('\f');
                        break;
                    default:
                        sbr.Append(ch);
                        break;
                }
            }

            return sbr.ToString();
        }
    }
}
