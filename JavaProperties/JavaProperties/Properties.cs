﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JavaProperties
{
    public class Properties : Dictionary<string, string>
    {
        public void Load(TextReader inputStream)
        {
            var reader = new PropertiesTextReader(inputStream);
            var parser = new PropertiesParser();

            string decl;
            while ((decl = reader.ReadDeclaration()) != null)
            {
                var kv = parser.ParseDeclaration(decl);
                if (kv.Key == null)
                {
                    continue;
                }

                this[kv.Key] = kv.Value;
            }
        }

        public Dictionary<string, object> Treeify(char separator = '.')
        {
            var root = new Dictionary<string, object>();

            foreach (var kv in this)
            {
                TreeifyBranch(kv.Key, kv.Value, root, separator);
            }

            return root;
        }

        private void TreeifyBranch(string key, string value, Dictionary<string, object> parent, char separator)
        {
            var i = key.IndexOf(separator);
            if (i == -1)
            {
                parent[key] = value;
                return;
            }

            var k = key.Substring(0, i);
            Dictionary<string, object> node;

            if (parent.ContainsKey(k))
            {
                node = parent[k] as Dictionary<string, object>;
                if (node == null)
                {
                    node = new Dictionary<string, object> { { "", parent[k] as string } };
                    parent.Add(k, node);
                }
            }
            else
            {
                node = new Dictionary<string, object>();
                parent.Add(k, node);
            }

            TreeifyBranch(key.Substring(i + 1), value, node, separator);
        }
    }
}
