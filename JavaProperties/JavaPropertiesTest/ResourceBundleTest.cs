﻿using JavaProperties.ResourceProvider;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace JavaPropertiesTest
{
    [TestClass]
    public class ResourceBundleTest
    {
        private ResourceBundle LoadFromAssembly()
        {
            return ResourceBundle.Load(Assembly.GetExecutingAssembly(), "test");
        }

        private ResourceBundle LoadFromFiles()
        {
            return ResourceBundle.Load(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), false, "test");
        }

        [TestMethod]
        public void LoadAssembly()
        {
            var bundle = LoadFromAssembly();
            Assert.IsNotNull(bundle);
            CollectionAssert.AreEqual(new[] { "", "en", "en-GB" }, bundle.Select(p => p.Key.Name).OrderBy(c => c).ToArray());
        }

        [TestMethod]
        public void LoadFiles()
        {
            var bundle = LoadFromFiles();
            Assert.IsNotNull(bundle);
            CollectionAssert.AreEqual(new[] { "", "en", "en-GB" }, bundle.Select(p => p.Key.Name).OrderBy(c => c).ToArray());
        }

        [TestMethod]
        public void SimpleKey()
        {
            var bundle = LoadFromAssembly();
            Assert.AreEqual("a(neutral)", bundle.Get("test.a", CultureInfo.InvariantCulture));
        }

        [TestMethod]
        public void LocalizedKey()
        {
            var bundle = LoadFromAssembly();
            Assert.AreEqual("a(neutral)", bundle.Get("test.a", new CultureInfo("en-GB")));
            Assert.AreEqual("b(en)", bundle.Get("test.b", new CultureInfo("en-GB")));
            Assert.AreEqual("c(en-GB)", bundle.Get("test.c", new CultureInfo("en-GB")));
            Assert.AreEqual("c(en)", bundle.Get("test.c", new CultureInfo("en")));
            Assert.AreEqual("c(neutral)", bundle.Get("test.c", CultureInfo.InvariantCulture));
        }

        [TestMethod]
        public void FallbackKey()
        {
            var bundle = LoadFromAssembly();
            Assert.AreEqual("c(neutral)", bundle.Get("test.c", new CultureInfo("ru")));
        }
    }
}
