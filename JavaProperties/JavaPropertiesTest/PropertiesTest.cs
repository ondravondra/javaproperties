﻿using JavaProperties;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JavaPropertiesTest
{
    [TestClass]
    public class PropertiesTest
    {
        [TestMethod]
        public void TestTreeify()
        {
            var prop = new Properties();
            prop.Add("a", "1");
            prop.Add("b.c.d", "2");
            prop.Add("b.c.e", "3");

            var tree = prop.Treeify();

            Assert.IsTrue(tree.ContainsKey("a"));
            Assert.IsTrue(tree.ContainsKey("b"));

            Assert.AreEqual("1", tree["a"]);

            var b = tree["b"] as IDictionary<string, object>;
            Assert.IsNotNull(b);
            Assert.AreEqual("c", string.Join("", b.Keys));

            var c = b["c"] as IDictionary<string, object>;
            Assert.IsNotNull(c);
            Assert.AreEqual("de", string.Join("", c.Keys.OrderBy(k => k)));

            Assert.AreEqual("2", c["d"]);
            Assert.AreEqual("3", c["e"]);
        }
    }
}
