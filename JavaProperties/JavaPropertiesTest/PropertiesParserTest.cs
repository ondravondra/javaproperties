﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using JavaProperties;

namespace JavaPropertiesTest
{
    [TestClass]
    public class PropertiesParserTest
    {
        [TestMethod]
        public void TestSepColon()
        {
            const string decl = "abcd:efgh";
            const string expKey = "abcd";
            const string expVal = "efgh";

            var kv = new PropertiesParser().ParseDeclaration(decl);
            Assert.AreEqual(expKey, kv.Key);
            Assert.AreEqual(expVal, kv.Value);
        }

        [TestMethod]
        public void TestSepEq()
        {
            const string decl = "abcd=efgh";
            const string expKey = "abcd";
            const string expVal = "efgh";

            var kv = new PropertiesParser().ParseDeclaration(decl);
            Assert.AreEqual(expKey, kv.Key);
            Assert.AreEqual(expVal, kv.Value);
        }

        [TestMethod]
        public void TestSepSpaces()
        {
            const string decl = "abcd   efgh";
            const string expKey = "abcd";
            const string expVal = "efgh";

            var kv = new PropertiesParser().ParseDeclaration(decl);
            Assert.AreEqual(expKey, kv.Key);
            Assert.AreEqual(expVal, kv.Value);
        }

        [TestMethod]
        public void TestWhitespaces()
        {
            const string decl = "abcd      =  efgh";
            const string expKey = "abcd";
            const string expVal = "efgh";

            var kv = new PropertiesParser().ParseDeclaration(decl);
            Assert.AreEqual(expKey, kv.Key);
            Assert.AreEqual(expVal, kv.Value);
        }

        [TestMethod]
        public void TestEscaped()
        {
            const string decl = "abcd = e\\nf\\rg\\th\\f\\u8005i";
            const string expKey = "abcd";
            const string expVal = "e\nf\rg\th\f者i";

            var kv = new PropertiesParser().ParseDeclaration(decl);
            Assert.AreEqual(expKey, kv.Key);
            Assert.AreEqual(expVal, kv.Value);
        }
    }
}
