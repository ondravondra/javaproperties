﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using JavaProperties;

namespace JavaPropertiesTest
{
    [TestClass]
    public class PropertiesTextReaderTest
    {
        [TestMethod]
        public void TestSimple()
        {
            var input = "abcd efg \\na";
            var expected = "abcd efg \\na";

            var re = new PropertiesTextReader(new StringReader(input));

            Assert.AreEqual(expected, re.ReadDeclaration());
            Assert.IsNull(re.ReadDeclaration());
        }

        [TestMethod]
        public void TestEmpty()
        {
            var input = "";

            var re = new PropertiesTextReader(new StringReader(input));

            Assert.IsNull(re.ReadDeclaration());
        }

        [TestMethod]
        public void TestBlank()
        {
            var input = "a \n   \n  c ";
            var expected = new[] { "a ", "c " };

            var re = new PropertiesTextReader(new StringReader(input));

            Assert.AreEqual(expected[0], re.ReadDeclaration());
            Assert.AreEqual(expected[1], re.ReadDeclaration());
            Assert.IsNull(re.ReadDeclaration());
        }

        [TestMethod]
        public void TestTwo()
        {
            var input = "firstline\\na\nsecondline\n";
            var expected = new[] { "firstline\\na", "secondline" };

            var re = new PropertiesTextReader(new StringReader(input));

            Assert.AreEqual(expected[0], re.ReadDeclaration());
            Assert.AreEqual(expected[1], re.ReadDeclaration());
            Assert.IsNull(re.ReadDeclaration());
        }

        [TestMethod]
        public void TestSplit()
        {
            var input = "firstline\\\n     \\\n   \\\n  abc\\\n  \nzzz";
            var expected = new[] { "firstlineabc", "zzz" };

            var re = new PropertiesTextReader(new StringReader(input));

            Assert.AreEqual(expected[0], re.ReadDeclaration());
            Assert.AreEqual(expected[1], re.ReadDeclaration());
            Assert.IsNull(re.ReadDeclaration());
        }
    }
}
